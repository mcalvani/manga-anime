<?php   
    $letters_array = array("ALL","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
    $array_length = count($letters_array);
?>
<div id="block_letter_list">
    <?php 
            for($i=0;$i<$array_length;$i++){ 
                if ($letters_array[$i]=='ALL'): ?>
                       <a class='single_letter selected' href='#'><?php echo $letters_array[$i]; ?></a>
               <?php else: ?>
                        <a class='single_letter' href='#'><?php echo $letters_array[$i]; ?></a>
               <?php endif; 
            }     
    ?>
</div>