<?php
/**
 * Template Name: List Page 
 *
 * This is the template that displays full width pages.
 * 
 * @package aThemes
 */

get_header();?>


<div id="cont_left"> 
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <h1 class="titolo"><?php the_title(); ?></h1>
            <br />
            
             <?php while ( have_posts() ) : the_post(); ?>
                <?php $id = get_the_ID();?>
                <script>var id_pagina = "<?php echo $id; ?>"</script>
                <?php endwhile; ?>
                <?php if ($id== 326):?>
                            <?php get_template_part('manga', 'rotator'); ?>
                <?php else : ?>
                            <?php get_template_part('anime', 'rotator'); ?>
                <?php endif; ?>    
                <div class='list_posts_by_letter'>
                    <?php get_template_part( 'block','letter_list'); ?>
                    <br />
                    <br />
                    <hr>
                    </div>
                    <div id='cont_list_posts_by_letter'>
                        <?php if ($id== 326):?>
                            <?php get_template_part( 'block','manga_title_list'); ?>
                        <?php else : ?>
                            <?php get_template_part('block','anime_title_list')?>
                        <?php endif; ?>     
                    </div>
         </div>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
</div>

