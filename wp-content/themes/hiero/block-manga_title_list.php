<div id="block_title_list">
    ALL
    <hr>
    <?php global $post;
    $args = array(
        'post_type' => 'list-manga',
        'post_status' => 'publish',
        'orderby'=> 'title',
        'order' => 'ASC'
    );
    $tposts = get_posts($args);
    
    foreach ($tposts as $post) : setup_postdata($post); ?>
        <div class="post_title">
            <a href="<?php echo get_permalink($post->post_id); ?>">
                <?php _e($post->post_title); ?>
            </a>
        </div>        
    <?php endforeach; ?>
    
</div>

