<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package aThemes
 */
?>
		</div>
	<!-- #main --></div>

	<?php
		/* A sidebar in the footer? Yep. You can can customize
		 * your footer with up to four columns of widgets.
		 */
		get_sidebar( 'footer' );
	?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="clearfix container">
			<div class="site-info">
				&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ) ?>. All rights reserved.
			</div>
                        <div class="site-credit">
                                     <nav id="main-navigation2" class="main-navigation2" role="navigation">
                                        <a href="#main-navigation2" class="nav-open">Menu</a>
                                        <a href="#" class="nav-close">Close</a>
                                        <?php wp_nav_menu( array('clearfix sf-menu', 'theme_location' => 'foot' ) ); ?>
                                     </nav>
                        </div>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>