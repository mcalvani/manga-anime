<div id="cycle_rotator_desktop" class="desktop">
    <div id="cycle_nav_desktop" class="desktop">
        <div id="pager-rotator_desktop" class="desktop">
           
        </div>
    </div>
    <ul class="rotator">
        <?php
            global $post;

            $args = array(
                'post_type'     => 'banner-rotator',
                'post_status'   => 'draft',
                'tax_query' => array(
                    array(
                    'taxonomy' => 'banner-category',
                    'field'    => 'id',
                    'terms'    => 3,
          ),
         )
            );
            $tposts = get_posts($args);
            
            
            foreach($tposts as $post) : setup_postdata($post); ?>
                <li style="list-style-type:none;">
                    <?php
                        // Recupero l'id del post collegato, in modo tale da poterlo linkare al cycle
                        $related_posts = MRP_get_related_posts( $post->ID , true );
                        $ID_related_post = $related_posts[0]->ID;
                    ?>
                    <div class="box_cycle cycle_title">
                        <?php if(isset($ID_related_post)): ?>
                                <a href="<?php echo get_permalink($ID_related_post); ?>">
                                    <?php _e($post->post_title) ?><br><br><font size="5.5"><?php _e($post->post_content); ?></font>
                                </a>
                        <?php else: ?>
                                <?php _e($post->post_title)._e($post->post_content); ?>
                        <?php endif; ?>
         
                    </div>
                    <div class="box_cycle cycle_img">
                        <?php if(isset($ID_related_post)): ?>
                                <a href="<?php echo get_permalink($ID_related_post); ?>">
                                    <?php the_post_thumbnail( 'banner' ); ?>
                                </a>
                        <?php else: ?>
                                <?php the_post_thumbnail( 'banner' ); ?>
                        <?php endif; ?>
                    </div>                    
                </li>
        <?php endforeach; ?>
    </ul>
</div>

