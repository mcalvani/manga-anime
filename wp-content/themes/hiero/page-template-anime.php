<?php
/**
 * Template Name: Anime Page
 *
 * This is the template that displays full width pages.
 * 
 * @package aThemes
 */
get_header();
?>

<div id="cont_left">

    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <div class="desktop2">
                <?php get_template_part('anime', 'rotator'); ?>
            </div>
            <?php while (have_posts()) : the_post(); ?>
                <?php //get_template_part('content', 'page'); ?>
                <?php
                // If comments are open or we have at least one comment, load up the comment template
                if (comments_open() || '0' != get_comments_number())
                    comments_template();
                ?>

            <?php endwhile; // end of the loop. ?>
        </div>
    </div>
        <?php get_sidebar(); ?>
        <?php get_footer(); ?>
</div>