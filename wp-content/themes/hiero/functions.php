<?php
/**
 * aThemes functions and definitions
 *
 * @package aThemes
 */
if (!function_exists('athemes_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     */
    function athemes_setup() {

        /**
         * Make theme available for translation
         * Translations can be filed in the /lang/ directory
         * If you're building a theme based on aThemes, use a find and replace
         * to change 'athemes' to the name of your theme in all the template files
         */
        load_theme_textdomain('athemes', get_template_directory() . '/lang');

        /**
         * Add default posts and comments RSS feed links to head
         */
        add_theme_support('automatic-feed-links');

        // Set the content width based on the theme's design and stylesheet.
        global $content_width;
        if (!isset($content_width)) {
            $content_width = 1200; /* pixels */
        }

        /**
         * Enable support for Post Thumbnails on posts and pages
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        add_image_size('thumb-small', 50, 50, true);
        add_image_size('thumb-medium', 300, 135, true);
        add_image_size('thumb-featured', 250, 175, true);
        add_image_size('banner', 1000, 350, true);
        add_image_size('manga', 500, 350, true);

        /**
         * This theme uses wp_nav_menu() in one location.
         */
        register_nav_menus(array(
            'main' => __('Main Menu', 'athemes'),
            'foot' => __('menu_footer', 'athemes'),
        ));
    }

endif; // athemes_setup
add_action('after_setup_theme', 'athemes_setup');

/**
 * Register widgetized area and update sidebar with default widgets
 */
function athemes_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar', 'athemes'),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title"><span>',
        'after_title' => '</span></h3>',
    ));
    register_sidebar(array(
        'name' => __('Header', 'athemes'),
        'id' => 'sidebar-2',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => __('Sub Footer 1', 'athemes'),
        'id' => 'sidebar-3',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title"><span>',
        'after_title' => '</span></h3>',
    ));
    register_sidebar(array(
        'name' => __('Sub Footer 2', 'athemes'),
        'id' => 'sidebar-4',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title"><span>',
        'after_title' => '</span></h3>',
    ));
    register_sidebar(array(
        'name' => __('Sub Footer 3', 'athemes'),
        'id' => 'sidebar-5',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title"><span>',
        'after_title' => '</span></h3>',
    ));
    register_sidebar(array(
        'name' => __('Sub Footer 4', 'athemes'),
        'id' => 'sidebar-6',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title"><span>',
        'after_title' => '</span></h3>',
    ));
}

add_action('widgets_init', 'athemes_widgets_init');

/**
 * Count the number of footer sidebars to enable dynamic classes for the footer
 *
 * @since aThemes 1.0
 */
function athemes_footer_sidebar_class() {
    $count = 0;

    if (is_active_sidebar('sidebar-3'))
        $count++;

    if (is_active_sidebar('sidebar-4'))
        $count++;

    if (is_active_sidebar('sidebar-5'))
        $count++;

    if (is_active_sidebar('sidebar-6'))
        $count++;

    $class = '';

    switch ($count) {
        case '1':
            $class = 'site-extra extra-one';
            break;
        case '2':
            $class = 'site-extra extra-two';
            break;
        case '3':
            $class = 'site-extra extra-three';
            break;
        case '4':
            $class = 'site-extra extra-four';
            break;
    }

    if ($class)
        echo 'class="' . $class . '"';
}

/**
 * Enqueue scripts and styles
 */
function athemes_scripts() {

    //Load the fonts
    wp_register_script('cycle', get_bloginfo('stylesheet_directory') . '/js/jquery-cycle.js', array('jquery'), '2.9995');
    wp_enqueue_script('cycle');
    wp_register_script('main', get_bloginfo('stylesheet_directory') . '/js/main.js', array('jquery'), '2.9995');
    wp_enqueue_script('main');
    $headings_font = esc_html(get_theme_mod('headings_fonts'));
    $body_font = esc_html(get_theme_mod('body_fonts'));
    if ($headings_font) {
        wp_enqueue_style('athemes-headings-fonts', '//fonts.googleapis.com/css?family=' . $headings_font);
    } else {
        wp_enqueue_style('athemes-headings-fonts', '//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700');
    }
    if ($body_font) {
        wp_enqueue_style('athemes-body-fonts', '//fonts.googleapis.com/css?family=' . $body_font);
    }

    wp_enqueue_style('athemes-glyphs', get_template_directory_uri() . '/css/athemes-glyphs.css');

    wp_enqueue_style('athemes-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('athemes-style', get_stylesheet_uri());

    wp_enqueue_script('athemes-bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('athemes-superfish', get_template_directory_uri() . '/js/superfish.js', array('jquery'));
    wp_enqueue_script('athemes-supersubs', get_template_directory_uri() . '/js/supersubs.js', array('jquery'));
    wp_enqueue_script('athemes-settings', get_template_directory_uri() . '/js/settings.js', array('jquery'));

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'athemes_scripts');

/**
 * Load html5shiv
 */
function athemes_html5shiv() {
    echo '<!--[if lt IE 9]>' . "\n";
    echo '<script src="' . esc_url(get_template_directory_uri() . '/js/html5shiv.js') . '"></script>' . "\n";
    echo '<![endif]-->' . "\n";
}

add_action('wp_head', 'athemes_html5shiv');

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Add social links on user profile page.
 */
require get_template_directory() . '/inc/user-profile.php';

/**
 * Add custom widgets
 */
require get_template_directory() . '/inc/custom-widgets.php';
/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
/**
 * Dynamic styles
 */
require get_template_directory() . '/styles.php';

///////////////////////////////////////////////////////////////////
// function to crop HTML text correctly without breaking html tags
///////////////////////////////////////////////////////////////////

function truncate($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
    if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        $total_length = strlen($ending);
        $open_tags = array();
        $truncate = '';
        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag
                } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== false) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag
                } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate'd text
                $truncate .= $line_matchings[1];
            }
            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
            if ($total_length + $content_length > $length) {
                // the number of characters which are left
                $left = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entities_length <= $left) {
                            $left--;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= substr($line_matchings[2], 0, $left + $entities_length);
                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }
            // if the maximum length is reached, get off the loop
            if ($total_length >= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space...
        $spacepos = strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    // add the defined ending to the text
    $truncate .= $ending;
    if ($considerHtml) {
        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            $truncate .= '</' . $tag . '>';
        }
    }
    return $truncate;
}

/**
 * Enables the Excerpt meta box in Page edit screen.
 */
function wpcodex_add_excerpt_support_for_pages() {
    add_post_type_support('page', 'excerpt');
}

add_action('init', 'wpcodex_add_excerpt_support_for_pages');


add_action('wp_ajax_load_letter', 'loadLetter_func');
add_action('wp_ajax_nopriv_load_letter', 'loadLetter_func');

function loadLetter_func() {
    $selected_letter = $_POST['letter'];
    $id_pagina = $_POST['idpagina'];
    echo $selected_letter;
    if ($id_pagina == 326) {
        $post_type = 'list-manga';
        reload_Query($selected_letter, $post_type);
    } else {
        $post_type = 'list-anime';
        reload_Query($selected_letter, $post_type);
    }
    die();
}

function reload_Query($selected_letter, $post_type) {
    global $wpdb;
    $first_char = $selected_letter;
  
    if($selected_letter=='ALL'){
        global $post;
        $args = array(
            'post_type' => $post_type,
            'post_status' => 'publish',
            'orderby'=> 'title',
            'order' => 'ASC'
        );
        $tposts = get_posts($args);

        ?>
        <hr>
        <?php
        foreach ($tposts as $post) : setup_postdata($post); 
        ?>
            <div class="post_title">
                <a href="<?php echo get_permalink($post->post_id); ?>">
                    <?php _e($post->post_title); ?>
                </a>
            </div> 
            <?php
        endforeach;
        wp_reset_query();
    }else{

        $postids = $wpdb->get_col($wpdb->prepare("
            SELECT      ID
            FROM        $wpdb->posts
            WHERE       SUBSTR($wpdb->posts.post_title,1,1) = %s
            ORDER BY    $wpdb->posts.post_title",$first_char));
        if ($postids) {
            $args = array(
                'post__in' => $postids,
                'post_type' => $post_type,
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'caller_get_posts' => 1
            );
            $my_query = null;
            $my_query = new WP_Query($args);
            
            ?>
            <hr>
            <?php
            if ($my_query->have_posts()) {
                while ($my_query->have_posts()) : $my_query->the_post(); ?>
                    <p><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to<?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
                    <?php
                endwhile;
            }
            else{
                echo 'Non ci sono manga con questa iniziale';
            }
            wp_reset_query();
        }
   }
}
