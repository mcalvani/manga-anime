jQuery(document).ready(function($){
        $('.single_letter').bind('click',function(event){
            event.preventDefault();
            $('.selected').removeClass('selected');
            var click_letter=$(this).text();
            var data = { action : 'load_letter' , letter: click_letter, idpagina:id_pagina} 
            jQuery.post('/wp-admin/admin-ajax.php' , data , function(response){
                jQuery('#cont_list_posts_by_letter').html(response);
            });
            $(this).addClass('selected');
        });
 });