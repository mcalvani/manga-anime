<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'mangaanime');

/** Nome utente del database MySQL */
define('DB_USER', 'root');

/** Password del database MySQL */
define('DB_PASSWORD', 'root');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8mb4');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**Definizione delle variabili che non consentono l'auto aggiornamento.*/
define( 'WP_AUTO_UPDATE_CORE', false );
define( 'AUTOMATIC_UPDATER_DISABLED', true );
/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0N@y1#gURY!h~2w8Hp(f$~X^B5pX/;,sCY;k;jf,Anu2~09>~xhaXx6X1`V_4.5>');
define('SECURE_AUTH_KEY',  'k5Q!$x$XFu$VTDOJr<JncP$~bvgyzK99AvmX9;rReiR,= 4)zeAPM3$0*/LyHZn>');
define('LOGGED_IN_KEY',    'Wo9uaJlW1?i)Yjbr]>7bEGZ:/k/*/*bs@@AtQ/5)jfpZ*/b(>%!Oqt75~`=o-<6T');
define('NONCE_KEY',        'mYoC<5=*d_WB+.>.FYv}-U(,8(XPY5+ A4aVposP{`I)U4.vw46k]#m`lQ<`H8BV');
define('AUTH_SALT',        '*S$,C+kR5:KrX;E;7SNWoySYs@;?<~k0&(ArV~hu(zLmqa,s2<}S1oHVy5<6uP15');
define('SECURE_AUTH_SALT', '~;&QCmOkzhE26x]Tkw)SebIF7H.`WPUsrBL)+x4C=<PHn1Rs^;Jdsi{ba%0g)5xL');
define('LOGGED_IN_SALT',   '3^NsINC;MC;O_XX4wBNWw+#G<H,(~QOBWit(GK5pOlJ~k.KQrkB1C_lvSiUb:Ox&');
define('NONCE_SALT',       '>g:GbdM>,M[8DEY=I#llm%?`ua)t^Ntl=)m$,0^I:;lB9;|ez)om %)ZXq]t@k#(');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');